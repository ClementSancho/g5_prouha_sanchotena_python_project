'''
This file contains several blocs of code to test the good implementation of bond investment classes.

In a first section, the user is invited to enter parameters of his choice for bond investments: amount invested and investment term.
The program ensure the selection of valid parameters (numerical amount and term, etc.) in a robust way.

In a second section, the aim is to plot the investment evolution over 50 years for both short-term and long-term bonds.

'''

from Bond import ST_Bond, LT_Bond       # Import ST_Bond and LT_Bond classes from Bond.py file
import matplotlib.pyplot as plt         # Import matplotlib library for plotting
import numpy as np                      # Import numpy library

'''
SECTION 1

Section dedicated to tests of good instantiation of ST_Bond and LT_Bond classes.

Example: Select an amount of $500 and a term of 5 years.

Expected outcome
-> short-term bond instantiation will be valid whereas long-term bond instantiation will not 
   (selected amount lower than minimum required invested amount for long-term bonds).

'''

# Input management through interaction with the user and error handling

flag = False    # Flag variable to control the validity of inputs selected by the user

# Let the user select the amount of the investment until he enters a valid amount (numerical).
while not flag:
    amount = input("Please select an investment amount:")
    try:
        amount = float(amount)
        flag = True
    except:
        # Error message
        print("Selected amount is not valid." + '\n' + "Please select a numerical amount (float or integer).")

flag = False

# Let the user select the term of the investment until he enters a valid term (numerical).
while not flag:
    term = input("Please select an investment term (in years):")
    try:
        term = float(term)
        flag = True
    except:
        # Error message
        print("Selected term is not valid." + '\n' + "Please select a numerical term (float or integer).")


# Instantiation of short-term and long-term bond investments

# Short-term bond instantiation with selected parameters

ST_Bond1 = ST_Bond(amount, term)
ST_Bond1.describe()
print(f"Earned interests: ${round(ST_Bond1.interests(ST_Bond1.getTerm()), 2)} after {ST_Bond1.getTerm()} years.")

# Long-term bond instantiation with selected parameters

LT_Bond1 = LT_Bond(amount, term)
LT_Bond1.describe()
print(f"Earned interests: ${round(LT_Bond1.interests(LT_Bond1.getTerm()), 2)} after {LT_Bond1.getTerm()} years.")




'''
SECTION 2

Plotting of the evolution of an investment equal to the minimum required amount for each bond, over an investment term of 50 years.

'''

# Initialization of investment variables with values of interest
ST_invest_amount = 250
LT_invest_amount = 1000
invest_term = 50

ST_Bond_invest = ST_Bond(ST_invest_amount, invest_term)    # Instantiation of the short-term bond
LT_Bond_invest = LT_Bond(LT_invest_amount, invest_term)    # Instantiation of the long-term bond

time = np.linspace(0, invest_term, 51)    # Investment time line (in years)
ST_invest_values = np.zeros(len(time))    # Vector of annual values for the short-term bond
LT_invest_values = np.zeros(len(time))    # Vector of annual values for the long-term bond

# Value of each bond at the end of each year over the investment period of 50 years
for index, t in enumerate(time):
    ST_invest_values[index] = ST_Bond_invest.value(t)
    LT_invest_values[index] = LT_Bond_invest.value(t)


# Plotting functions
plt.figure(figsize=(15,6))
plt.title("Investment evolution for short-term and long-term bonds")
plt.plot(time, ST_invest_values, label='Short-term bond', color='b')
plt.plot(time, LT_invest_values, label='Long-term bond', color='r')
plt.xlabel('Years')
plt.ylabel('Investment value ($)')
plt.grid()
plt.legend()
plt.show()










