
'''
This file runs the simulations to answer Bonus questions.

'''

from Investors import Investor, years           # Import Investor class and years vector from Investors.py file
from Stocks_Data import Stock_Prices            # Import stock prices dataframe from the file Stocks_Data.py
import numpy as np                              # Import numpy library
import pandas as pd                             # Import pandas library
import matplotlib.pyplot as plt                 # Import matplotlib library for plotting
from Stocks import Stock                        # Import Stock class from Stocks.py file


'''
Bonus Question 1
'''

nb_simulations = 500
initial_capital = 5000

n = len(years)

aggressive_investments = np.zeros(nb_simulations)
aggressive_investments_annual_performance = np.zeros((nb_simulations, n))
aggressive_investments_annual_return = np.zeros((nb_simulations, n-1))

for i in range(nb_simulations):
    aggressive_investor = Investor(initial_capital, "aggressive").invest()
    aggressive_investments[i] = aggressive_investor[0]
    for j in range(n):
        aggressive_investments_annual_performance[i, j] = aggressive_investor[1][j]
        if j >= 1:
            aggressive_investments_annual_return[i, j-1] = aggressive_investments_annual_performance[i, j]/aggressive_investments_annual_performance[i, j-1] - 1

average_aggressive_annual_performance = np.mean(aggressive_investments_annual_performance, axis=0)
average_aggressive_annual_return = np.mean(aggressive_investments_annual_return, axis=0)

max_return = max(average_aggressive_annual_return)
index_max_return = np.argsort(average_aggressive_annual_return)[-1]
index_best_year = index_max_return + 1

print(f"On average, the best year for an aggressive investor is {years[index_best_year].year}, with an average annual return of {round(max_return*100, 2)}%.")


'''
Bonus Question 2

Remark: Configuration 1 by default
'''


nb_simulations = 500

n = len(years)

defensive_investments = np.zeros(nb_simulations)
defensive_investments_annual_performance = np.zeros((nb_simulations, n))
aggressive_investments = np.zeros(nb_simulations)
aggressive_investments_annual_performance = np.zeros((nb_simulations, n))
mixed_investments = np.zeros(nb_simulations)
mixed_investments_annual_performance = np.zeros((nb_simulations, n))


for i in range(nb_simulations):
    initial_capital = int(np.random.normal(20000, 5000))
    defensive_investor = Investor(initial_capital, "defensive").invest()
    aggressive_investor = Investor(initial_capital, "aggressive").invest()
    mixed_investor = Investor(initial_capital, "mixed").invest()
    defensive_investments[i] = defensive_investor[0]
    aggressive_investments[i] = aggressive_investor[0]
    mixed_investments[i] = mixed_investor[0]
    for j in range(n):
        defensive_investments_annual_performance[i, j] = defensive_investor[1][j]
        aggressive_investments_annual_performance[i, j] = aggressive_investor[1][j]
        mixed_investments_annual_performance[i, j] = mixed_investor[1][j]

average_defensive_final_capital = np.mean(defensive_investments)
average_aggressive_final_capital = np.mean(aggressive_investments)
average_mixed_final_capital = np.mean(mixed_investments)

average_defensive_return = average_defensive_final_capital/initial_capital - 1
average_aggressive_return = average_aggressive_final_capital/initial_capital - 1
average_mixed_return = average_mixed_final_capital/initial_capital - 1

average_defensive_annual_performance = np.mean(defensive_investments_annual_performance, axis=0)
average_aggressive_annual_performance = np.mean(aggressive_investments_annual_performance, axis=0)
average_mixed_annual_performance = np.mean(mixed_investments_annual_performance, axis=0)

print(f"The average capital of 500 defensive investors at the end of the investment period (01/01/2005 to 12/31/2020)"
      f" is ${round(average_defensive_final_capital, 2)}, i.e. {round(average_defensive_return*100, 2)}%.")
print(f"The average capital of 500 aggressive investors at the end of the investment period (01/01/2005 to 12/31/2020)"
      f" is ${round(average_aggressive_final_capital, 2)}, i.e. {round(average_aggressive_return*100, 2)}%.")
print(f"The average capital of 500 mixed investors at the end of the investment period (01/01/2005 to 12/31/2020)"
      f" is ${round(average_mixed_final_capital, 2)}, i.e. {round(average_mixed_return*100, 2)}%.")



plt.figure(figsize=(15,6))
plt.title("Average annual performance with respect to investment mode")
plt.plot(years, average_defensive_annual_performance, label='Defensive investors', color='b')
plt.plot(years, average_aggressive_annual_performance, label='Aggressive investors', color='r')
plt.plot(years, average_mixed_annual_performance, label='Mixed investors (config 1)', color='g')
plt.xlabel('Years')
plt.ylabel('Investment performance ($)')
plt.grid()
plt.legend()
plt.show()


'''
Bonus Question 3
'''

invest_start_date = '01/01/2007'                                    # Initial date of investment period
invest_start_date = pd.to_datetime(invest_start_date).date()        # Conversion to datetime format
invest_term = 365                                                   # Investment over 1 year (365 days)
stocks = Stock_Prices.columns                                       # List of stocks

annual_return = np.zeros(len(stocks))

for i in range(len(stocks)):
    investment = Stock(stocks[i], 1000, invest_start_date, invest_term)
    annual_return[i] = investment.getInvestmentReturn()

best_2007_return = max(annual_return)
best_2007_return_index = np.argsort(annual_return)[-1]

print(f"In 2007, the best performing stock was {stocks[best_2007_return_index]}, with an annual return of {round(best_2007_return*100, 2)}%.")

