
'''
This file contains classes and their attributes and methods for investor modelling.

GENERAL DESCRIPTION

Investor objects have the following attributes:
   - an initial capital (budget)
   - an investment mode: defensive, aggressive or mixed
   - a configuration for type of mixed investment (only useful for mixed investment strategy, more on this later)
   - a validity check which aims to characterize the validity or invalidity of the investor's status

An investor's status will be considered as "valid" if:
   - the initial capital is numerical and positive.
   - the selected mode belongs to the list of eligible investment modes,
     i.e. [defensive, aggressive, mixed]
   - the configuration is 1, 2 or 3

Otherwise the investment is considered as "not valid".


INVESTMENT MODE DESCRIPTION

   * DEFENSIVE INVESTMENT MODE

Defensive investors only invest in bonds.

First, their initial capital is randomly split as long as the remaining capital is greater than the minimum investment
amount allowed for short-term bonds (i.e. $250).
Then, each "subcapital" is invested in either long-term or short-term bonds, with equal probability (50%-50%),
provided that the subcapital in question is greater than the minimum investment amount allowed for long-term bonds (i.e. $1000).
If any randomly obtained subcapital turns out to be lower than the minimum investment amount allowed for long-term bonds (i.e. $1000)
but still greater than the minimum investment amount allowed for short-term bonds (i.e. $250), then it is automatically invested
in a short-term bond.
If any randomly obtained subcapital turns out to be lower than the minimum investment amount allowed for short-term bonds (i.e. $250),
then it is kept not invested until the final date of investment period (i.e. 2020-12-31).

For each bond investment, the term (in years) is also selected randomly, between the minimum investment term respective to each type of bond
(short-term or long-term with respective minimum term of 2 and 5 years) and the maximum term until the final date of investment period.
If the maximum term until the final date of investment period is lower than the minimum term for long-term bonds, then the subcapital
is automatically invested in a short-term bond.
If the maximum term until the final date of investment period is lower than the minimum term for short-term bonds,
then the subcapital is kept not invested until the final date of investment period (i.e. 2020-12-31).

Any time an investment reaches its term, it is reinvested randomly in either short-term or long-term bonds,
following the process described above.


   * AGGRESSIVE INVESTMENT MODE

Aggressive investors only invest in stocks.

First, their initial capital is randomly split as long as the remaining capital is greater than $100.
Then, each "subcapital" is invested in a randomly picked stock (uniformly drawn in the list of eligible stocks).

For each stock investment, the term (in days) is also selected randomly, between 1 day and the maximum term until
the final date of investment period.

Any time an investment reaches its term, it is reinvested randomly following the process described above.


   * MIXED INVESTMENT MODE

Mixed investors in both bonds and stocks.

First, their initial capital is randomly split as long as the remaining capital is greater than the minimum investment
amount allowed for short-term bonds (i.e. $250).

Then, each "subcapital" is randomly invested following either the defensive strategy or the aggressive strategy.

As a first step, any subcapital has 25% chances to be invested in bonds (defensive stategy) and 75% chances to be invested
in stocks (aggressive strategy).

 * CONFIGURATION 1
A first version for mixed investment strategy (Part3, configuration 1) consider that once an investment is made on bonds or stock,
then it continues to be invested in the same class of securities until the final date of investment period.

 * CONFIGURATION 2
A second version for mixed investment strategy (Part4, configuration 2) consider that each reinvestment is randomly allocated in either
bonds or stocks. Again, each reinvestment has 25% chances to be made in bonds (defensive stategy) and 75% chances to be made
in stocks (aggressive strategy).

 * CONFIGURATION 3
A third version for mixed investment strategy (Part4, configuration 3) is the same than version 2, but with a reverse probability
to invest in bonds or stocks at each reinvestment (i.e. 75% chances to invest in bonds and 25% chances to invest in stocks).

The construction of such investment process being quite complex, elementary functions are built outside the Investor class
to implement the different steps of the investment process described above.

Other "get" and "set" functions are provided, and a describe() function aims to print the characteristics of each investor instatiation.

'''

from Bond import Bond, ST_Bond, LT_Bond            # Import ST_Bond and LT_Bond classes from Bond.py file
from Stocks import Stock                     # Import Stock class from Stocks.py file
from Stocks_Data import Stock_Prices         # Import stock prices dataframe from the file Stocks_Data.py
from datetime import timedelta               # Import specific function useful for the date management under datetime format
import numpy as np                           # Import numpy library
import pandas as pd                       # Import pandas library
import random as rd                          # Import random library


# Global variables (useful in the entire Investors file)

ST_min_investment = 250                                             # Minimum investment amount allowed for short-term bonds: $250
ST_min_term = 2                                                     # Minimum investment term allowed for short-term bonds: 2 years
LT_min_investment = 1000                                            # Minimum investment amount allowed for long-term bonds: $1000
LT_min_term = 5                                                     # Minimum investment term allowed for long-term bonds: 5 years
stock_min_amount = 100                                              # Minimum investment amount allowed for stock investment: $100
invest_start_date = '01/01/2005'                                    # Initial date of investment period
invest_start_date = pd.to_datetime(invest_start_date).date()        # Conversion to datetime format
invest_end_date = '12/31/2020'                                      # Final date of investment period
invest_end_date = pd.to_datetime(invest_end_date).date()            # Conversion to datetime format
modes = ["defensive", "aggressive", "mixed"]                        # Investment modes

# Creation of the vector of year-end dates over the investment period
# Useful for the annual performance tracker
years = []
years.append(invest_start_date)                            # Initialization with the investment start date 2005-01-01
start_year = invest_start_date.year
end_year = invest_end_date.year
for year in range(start_year, end_year+1):
    year_end_date = '12/31/'+str(year)
    years.append(pd.to_datetime(year_end_date).date())



# ELEMENTARY FUNCTIONS

# Return the number of days between two dates given as arguments
def delta_days(start,end):
    delta = end - start
    return delta.days

# Return a randomly picked bond type (short-term or long-term), with equal probability
def Bond_Selector():
    threshold = 0.5
    rand_num = np.random.uniform(0,1)
    if rand_num < threshold:
        return "short-term"
    else:
        return "long-term"

# Return a randomly picked ticker (uniformly drawn) among eligible stocks
def Stock_Selector():
    n = len(Stock_Prices.columns)
    rand_index = rd.randint(0, n-1)
    return Stock_Prices.columns[rand_index]

# Return a randomly chosen investment strategy between defensive and aggressive strategies
# The probability (threshold) to draw the aggressive strategy has to be entered as argument (0.75 by default)
def Investment_Selector(stock_threshold=0.75):
    rand_num = np.random.uniform(0,1)
    if rand_num < stock_threshold:
        return "Stock"
    else:
        return "Bond"

# Return a list of "subcapitals" from the random split of an initial capital given as argument
def investment_random_split(capital, threshold):
    investment_split = []
    capital_left = capital
    while capital_left > threshold:
        investment_split.append([rd.randint(threshold, capital_left), invest_start_date])
        capital_left -= investment_split[-1][0]
    investment_split.append([capital_left, invest_start_date])
    return investment_split


# Function for random investment in bonds (elementary investment step for defensive strategy)
# Takes three arguments: the initial capital for the investment, the investment start date
# and the vector of annual performances
# Return a list of three elements:
# 1. Return the final capital at the end of the investment
# 2. Return the final term of the investment
# 3. Return the updated annual performance vector including value of the realized investment

def random_bond_invest(capital, start_date, investment_annual_values):

    n = len(investment_annual_values)
    max_term_days = delta_days(start_date, invest_end_date)
    max_term_year = max_term_days / 365

    if capital >= LT_min_investment and max_term_year >= LT_min_term:
        bond_type = Bond_Selector()
    elif capital >= ST_min_investment and max_term_year >= ST_min_term:
        bond_type = "short-term"
    else:
        bond_type = "not valid"

    if bond_type == "short-term":
        invest_term = rd.randint(ST_min_term * 365, max_term_days) / 365
        investment = ST_Bond(capital, invest_term)
        invest_term_date = start_date + timedelta(int(investment.getTerm() * 365))
        invest_final_value = investment.value(investment.getTerm())
        for i in range(1, n):
            if start_date <= years[i] and years[i] <= invest_term_date:
                investment_annual_values[i] += investment.value(delta_days(start_date, years[i]) / 365)
        return [invest_final_value, invest_term_date, investment_annual_values]
    elif bond_type == "long-term":
        invest_term = rd.randint(LT_min_term * 365, max_term_days) / 365
        investment = LT_Bond(capital, invest_term)
        invest_term_date = start_date + timedelta(int(investment.getTerm() * 365))
        invest_final_value = investment.value(investment.getTerm())
        for i in range(1, n):
            if start_date <= years[i] and years[i] <= invest_term_date:
                investment_annual_values[i] += investment.value(delta_days(start_date, years[i]) / 365)
        return [invest_final_value, invest_term_date, investment_annual_values]
    else:
        for i in range(1, n):
            if years[i] > start_date:
                investment_annual_values[i] += capital
        return [capital, invest_end_date, investment_annual_values]


# Function for random investment in stocks (elementary investment step for aggressive strategy)
# Takes three arguments: the initial capital for the investment, the investment start date
# and the vector of annual performances
# Return a list of three elements:
# 1. Return the final capital at the end of the investment
# 2. Return the final term of the investment
# 3. Return the updated annual performance vector including value of the realized investment

def random_stock_invest(capital, start_date, investment_annual_values):

    n = len(investment_annual_values)
    max_term_days = delta_days(start_date, invest_end_date)

    if capital >= stock_min_amount and start_date < invest_end_date:
        stock = Stock_Selector()
    else:
        stock = "no investment"

    if stock in Stock_Prices.columns:
        invest_term = rd.randint(1, max_term_days)
        investment = Stock(stock, capital, start_date, invest_term)
        invest_term_date = investment.getInvestEndDate()
        invest_final_value = investment.getSellingPrice() * investment.getNumberOfStocks()

        # for each year-end date between investment start and end dates, add the value of the current investment at the
        # year-end date in question
        for i in range(1, n):
            if start_date <= years[i] and years[i] <= invest_term_date:
                investment_annual_values[i] += investment.getNumberOfStocks() * investment.getPrice(years[i])
        return [invest_final_value, invest_term_date, investment_annual_values]
    else:
        for i in range(1, n):
            if years[i] > start_date:
                investment_annual_values[i] += capital
        return [capital, invest_end_date, investment_annual_values]



# Implement the defensive investment strategy according to the investment mode description given in the class description
# Take the initial capital as argument and return the total capital at the end of the investment period
# as well as the associated annual performances over this period

def defensive_strategy(capital):
    investment_annual_values = np.zeros(len(years))
    investment_annual_values[0] = capital
    investment_split = investment_random_split(capital, ST_min_investment)
    n = len(investment_split)
    total_capital = 0
    for i in range(n):
        while investment_split[i][1] < invest_end_date:
            cap_invest = investment_split[i][0]
            start_date = investment_split[i][1]
            random_invest = random_bond_invest(cap_invest, start_date, investment_annual_values)
            investment_split[i] = [random_invest[0], random_invest[1]]
            investment_annual_values = random_invest[2]
        total_capital += investment_split[i][0]
    return [total_capital, investment_annual_values]


# Implement the aggressive investment strategy according to the investment mode description given in the class description
# Take the initial capital as argument and return the total capital at the end of the investment period
# as well as the associated annual performances over this period

def aggressive_strategy(capital):
    investment_annual_values = np.zeros(len(years))
    investment_annual_values[0] = capital
    investment_split = investment_random_split(capital, stock_min_amount)
    n = len(investment_split)
    total_capital = 0
    for i in range(n):
        while investment_split[i][1] < invest_end_date:
            cap_invest = investment_split[i][0]
            start_date = investment_split[i][1]
            random_invest = random_stock_invest(cap_invest, start_date, investment_annual_values)
            investment_split[i] = [random_invest[0], random_invest[1]]
            investment_annual_values = random_invest[2]
        total_capital += investment_split[i][0]
    return [total_capital, investment_annual_values]


# Implement the mixed investment strategy according to the investment mode description given in the class description
# Mixed strategy under configuration 1 (Part 3, see class description)
# Take the initial capital as argument and return the total capital at the end of the investment period
# as well as the associated annual performances over this period

def mixed_strategy_v1(capital):
    investment_annual_values = np.zeros(len(years))
    investment_annual_values[0] = capital
    investment_split = investment_random_split(capital, ST_min_investment)
    n = len(investment_split)
    total_capital = 0
    for i in range(n):
        investment_type = Investment_Selector()
        if investment_type == "Stock":
            while investment_split[i][1] < invest_end_date:
                cap_invest = investment_split[i][0]
                start_date = investment_split[i][1]
                random_invest = random_stock_invest(cap_invest, start_date, investment_annual_values)
                investment_split[i] = [random_invest[0], random_invest[1]]
                investment_annual_values = random_invest[2]
        else:
            while investment_split[i][1] < invest_end_date:
                cap_invest = investment_split[i][0]
                start_date = investment_split[i][1]
                random_invest = random_bond_invest(cap_invest, start_date, investment_annual_values)
                investment_split[i] = [random_invest[0], random_invest[1]]
                investment_annual_values = random_invest[2]
        total_capital += investment_split[i][0]
    return [total_capital, investment_annual_values]

# Implement the mixed investment strategy according to the investment mode description given in the class description
# Mixed strategy under configuration 2 (Part 4, see class description)
# Take the initial capital as argument and return the total capital at the end of the investment period
# as well as the associated annual performances over this period

def mixed_strategy_v2(capital):
    investment_annual_values = np.zeros(len(years))
    investment_annual_values[0] = capital
    investment_split = investment_random_split(capital, ST_min_investment)
    n = len(investment_split)
    total_capital = 0
    for i in range(n):
        while investment_split[i][1] < invest_end_date:
            cap_invest = investment_split[i][0]
            start_date = investment_split[i][1]
            investment_type = Investment_Selector()
            if investment_type == "Stock":
                random_invest = random_stock_invest(cap_invest, start_date, investment_annual_values)
                investment_split[i] = [random_invest[0], random_invest[1]]
                investment_annual_values = random_invest[2]
            else:
                random_invest = random_bond_invest(cap_invest, start_date, investment_annual_values)
                investment_split[i] = [random_invest[0], random_invest[1]]
                investment_annual_values = random_invest[2]
        total_capital += investment_split[i][0]
    return [total_capital, investment_annual_values]


# Implement the mixed investment strategy according to the investment mode description given in the class description
# Mixed strategy under configuration 3 (Part 4, see class description)
# Take the initial capital as argument and return the total capital at the end of the investment period
# as well as the associated annual performances over this period

def mixed_strategy_v3(capital):
    investment_annual_values = np.zeros(len(years))
    investment_annual_values[0] = capital
    investment_split = investment_random_split(capital, ST_min_investment)
    n = len(investment_split)
    total_capital = 0
    for i in range(n):
        while investment_split[i][1] < invest_end_date:
            cap_invest = investment_split[i][0]
            start_date = investment_split[i][1]
            investment_type = Investment_Selector(0.25)
            if investment_type == "Stock":
                random_invest = random_stock_invest(cap_invest, start_date, investment_annual_values)
                investment_split[i] = [random_invest[0], random_invest[1]]
                investment_annual_values = random_invest[2]
            else:
                random_invest = random_bond_invest(cap_invest, start_date, investment_annual_values)
                investment_split[i] = [random_invest[0], random_invest[1]]
                investment_annual_values = random_invest[2]
        total_capital += investment_split[i][0]
    return [total_capital, investment_annual_values]




class Investor(object):

    # Attibute initialization
    capital = 0           # Investor's initial capital (budget)
    mode = ""             # Investor's investment mode
    config = 1            # Mixed mode configuration (useful only for mixed investors)
    is_valid = False      # Check investor's status validity

    # Constructor

    def __init__(self, capital, mode, config=1):

        # Check of selected capital validity for the investor
        if (not isinstance(capital, float)) and (not isinstance(capital, int)):
            pass  # Instruction to do nothing
        elif (capital < 0):
            pass  # Instruction to do nothing

        # Check of selected mode validity for the investor
        elif mode not in modes:
            pass  # Instruction to do nothing

        # Check of selected mixed mode configuration validity
        elif config != 1 and config != 2 and config != 3:
            pass  # Instruction to do nothing

        else:
            self.capital = capital
            self.mode = mode
            self.config = config
            self.is_valid = True


    # Description ("print") function
    # Describe and print the main characteristics of the investor
    def describe(self):
        if self.is_valid:
            print(f"This is a {self.mode} investor owning an initial capital of ${round(self.capital, 2)}.")
        else:
            print("This investor's status is not valid.")


    # "Get" functions (capital, mode)

    # Return the initial capital of the investor
    def getCapital(self):
        if self.is_valid:
            return self.capital

    # Return the investment mode of the investor
    def getMode(self):
        return self.mode

    # Return the configuration of the mixed strategy of the investor
    def getConfig(self):
        return self.config

    # "Set" functions (mode, configuration)

    # Set a new investment mode for the investor
    def setMode(self, mode):
        self.mode = mode

    # Set a new configuration for the mixed investment mode for the investor
    def setConfig(self, config):
        self.config = config

    # Investment function
    # Return the outcome of the investment strategy according to investor's investment mode
    def invest(self):
        if self.is_valid:
            if self.mode == "defensive":
                return defensive_strategy(self.capital)
            elif self.mode == "aggressive":
                return aggressive_strategy(self.capital)
            else:
                if self.config == 1:
                    return mixed_strategy_v1(self.capital)
                elif self.config == 2:
                    return mixed_strategy_v2(self.capital)
                else:
                    return mixed_strategy_v3(self.capital)

