

import matplotlib.pyplot as plt           # Import matplotlib library for plotting
from Investors import Investor            # Import Stock class from Stocks.py file
from Stocks_Data import Stock_Prices      # Import stock prices dataframe from the file Stocks_Data.py
import numpy as np                        # Import numpy library
import pandas as pd                       # Import pandas library

'''
PART 3
'''

nb_simulations = 500
initial_capital = 5000

defensive_investments = np.zeros(nb_simulations)
aggressive_investments = np.zeros(nb_simulations)
mixed_investments = np.zeros(nb_simulations)

for i in range(nb_simulations):
    defensive_investor = Investor(initial_capital, "defensive")
    aggressive_investor = Investor(initial_capital, "aggressive")
    mixed_investor = Investor(initial_capital, "mixed")
    defensive_investments[i] = defensive_investor.invest()[0]
    aggressive_investments[i] = aggressive_investor.invest()[0]
    mixed_investments[i] = mixed_investor.invest()[0]

average_defensive_final_capital = np.mean(defensive_investments)
average_aggressive_final_capital = np.mean(aggressive_investments)
average_mixed_final_capital = np.mean(mixed_investments)

average_defensive_return = average_defensive_final_capital/initial_capital - 1
average_aggressive_return = average_aggressive_final_capital/initial_capital - 1
average_mixed_return = average_mixed_final_capital/initial_capital - 1

print(f"The average capital of 500 defensive investors at the end of the investment period (01/01/2005 to 12/31/2020)"
      f" is ${round(average_defensive_final_capital, 2)}, i.e. {round(average_defensive_return*100, 2)}%.")
print(f"The average capital of 500 aggressive investors at the end of the investment period (01/01/2005 to 12/31/2020)"
      f" is ${round(average_aggressive_final_capital, 2)}, i.e. {round(average_aggressive_return*100, 2)}%.")
print(f"The average capital of 500 mixed investors at the end of the investment period (01/01/2005 to 12/31/2020)"
      f" is ${round(average_mixed_final_capital, 2)}, i.e. {round(average_mixed_return*100, 2)}%.")

