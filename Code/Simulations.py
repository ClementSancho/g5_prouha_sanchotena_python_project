
'''
This file runs the simulations to answer Part 4 questions.

'''
import matplotlib.pyplot as plt                 # Import matplotlib library for plotting
from Investors import Investor, years           # Import Investor class and years vector from Investors.py file
from Stocks_Data import Stock_Prices            # Import stock prices dataframe from the file Stocks_Data.py
import numpy as np                              # Import numpy library
import pandas as pd                             # Import pandas library



'''
PART 4

To answer question 1, please select 1 as configuration

To answer question 2 a), please select 2 as configuration

To answer question 2 b), please select 3 as configuration

To answer question 3, please select the initial capital multiplier
'''

# Input management through interaction with the user and error handling

flag = False    # Flag variable to control the validity of inputs selected by the user

# Let the user select the configuration of mixed investment strategy.
while not flag:
    config = input("Please select a mixed investment configuration (1, 2 or 3):")
    try:
        config = int(config)
        if config == 1 or config == 2 or config == 3:
            flag = True
        else:
            print("Selected configuration is not valid.")
    except:
        # Error message
        print("Selected configuration is not valid.")

flag = False

# Let the user select the initial capital multiplier
while not flag:
    multiplier = input("Please select an initial capital multiplier (integer between 1 and 10):")
    try:
        multiplier = int(multiplier)
        if multiplier >= 1 and multiplier <= 10:
            flag = True
        else:
            print("Selected configuration is not valid.")
    except:
        # Error message
        print("Selected configuration is not valid.")



nb_simulations = 500
initial_capital = 5000*multiplier

n = len(years)

defensive_investments = np.zeros(nb_simulations)
defensive_investments_annual_performance = np.zeros((nb_simulations, n))
aggressive_investments = np.zeros(nb_simulations)
aggressive_investments_annual_performance = np.zeros((nb_simulations, n))
mixed_investments = np.zeros(nb_simulations)
mixed_investments_annual_performance = np.zeros((nb_simulations, n))


for i in range(nb_simulations):
    defensive_investor = Investor(initial_capital, "defensive").invest()
    aggressive_investor = Investor(initial_capital, "aggressive").invest()
    mixed_investor = Investor(initial_capital, "mixed", config).invest()
    defensive_investments[i] = defensive_investor[0]
    aggressive_investments[i] = aggressive_investor[0]
    mixed_investments[i] = mixed_investor[0]
    for j in range(n):
        defensive_investments_annual_performance[i, j] = defensive_investor[1][j]
        aggressive_investments_annual_performance[i, j] = aggressive_investor[1][j]
        mixed_investments_annual_performance[i, j] = mixed_investor[1][j]

average_defensive_final_capital = np.mean(defensive_investments)
average_aggressive_final_capital = np.mean(aggressive_investments)
average_mixed_final_capital = np.mean(mixed_investments)

average_defensive_return = average_defensive_final_capital/initial_capital - 1
average_aggressive_return = average_aggressive_final_capital/initial_capital - 1
average_mixed_return = average_mixed_final_capital/initial_capital - 1

average_defensive_annual_performance = np.mean(defensive_investments_annual_performance, axis=0)
average_aggressive_annual_performance = np.mean(aggressive_investments_annual_performance, axis=0)
average_mixed_annual_performance = np.mean(mixed_investments_annual_performance, axis=0)

print(f"The average capital of 500 defensive investors at the end of the investment period (01/01/2005 to 12/31/2020)"
      f" is ${round(average_defensive_final_capital, 2)}, i.e. {round(average_defensive_return*100, 2)}%.")
print(f"The average capital of 500 aggressive investors at the end of the investment period (01/01/2005 to 12/31/2020)"
      f" is ${round(average_aggressive_final_capital, 2)}, i.e. {round(average_aggressive_return*100, 2)}%.")
print(f"The average capital of 500 mixed investors at the end of the investment period (01/01/2005 to 12/31/2020)"
      f" is ${round(average_mixed_final_capital, 2)}, i.e. {round(average_mixed_return*100, 2)}%.")



plt.figure(figsize=(15,6))
plt.title("Average annual performance with respect to investment mode")
plt.plot(years, average_defensive_annual_performance, label='Defensive investors', color='b')
plt.plot(years, average_aggressive_annual_performance, label='Aggressive investors', color='r')
plt.plot(years, average_mixed_annual_performance, label='Mixed investors (config '+str(config)+')', color='g')
plt.xlabel('Years')
plt.ylabel('Investment performance ($)')
plt.grid()
plt.legend()
plt.show()









