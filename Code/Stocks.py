
'''
This file contains classes and their attributes and methods for stock investments.

DESCRIPTION

Stock objects (investments) have the following attributes:
   - a stock name (ticker) among the following list: FDX, GOOGL, IBM, KO, MS, NOK, XOM
   - an investment start date
   - an investment term (in days)
   - a validity check which aims to characterize the validity or invalidity of the investment

A Stock investment will be considered as "valid" if:
   - the selected stock (ticker) belongs to the stock list available for investment,
     i.e. [FDX, GOOGL, IBM, KO, MS, NOK, XOM]
   - the selected amount is numerical and positive.
   - the selected investment start date is entered under the mm/dd/yyyy
   - the selected term (in days) is numerical (integer) and positive.

Otherwise the investment is considered as "not valid".

A specific function (adj_date()) manage buying and selling dates for investments as follows:
   - If a selected date is below the lower bound of allowed investment date range (i.e. 2005-01-01)
     (resp. exceed the upper bound of allowed investment date range (i.e. 2020-12-31)),
     then the first eligible business day (resp. the last eligible business day) is chosen by default.
   - If an investment start date (resp. investment end date) is not a business day,
     then the closest business day before the selected date is chosen as buying date (resp. selling date)


Remark:
Allowed investment period: from 2005-01-01 to 2020-12-31
Investment start date < lower bound of allowed investment date range will be changed into 2005-01-03 by default (adj_date function)
Investment start date > upper bound of allowed investment date range will be changed into 2020-12-31 by default (adj_date function)

Other "get" functions are provided, and a describe() function aims to print the characteristics of Stock investment instantiations.

'''

import  numpy as np                          # Import pandas library
import pandas as pd                          # Import pandas library
from Stocks_Data import Stock_Prices         # Import stock prices dataframe from the file Stocks_Data.py
from datetime import timedelta               # Import specific function useful for the date management under datetime format

min_invest_date = Stock_Prices.index.min()   # Lower bound of allowed investment date range
max_invest_date = Stock_Prices.index.max()   # Upper bound of allowed investment date range

# Date adjustment function (as described in the description)

def adj_date(date):
    # if date < lower bound of allowed investment date range
    # then adj_date = lower bound of allowed investment date range by default
    if (date < min_invest_date):
        return min_invest_date
    # if date > upper bound of allowed investment date range
    # then adj_date = upper bound of allowed investment date range by default
    elif (date > max_invest_date):
        return max_invest_date
    else:
        # if date in list of business days
        # then adj_date = date
        if date in Stock_Prices.index:
            return date
        else:
            # if date not in list of business days
            # then adj_date = closest business day before date
            while date not in Stock_Prices.index:
                date = date - timedelta(1)
            return date


class Stock(object):

    is_valid = False  # Check the investment validity

    # Constuctor

    # Ensure a correct instantiation of Stock objects with valid parameters
    # If invalid parameters selected, set the investment as "not valid", and return an error message
    def __init__(self, ticker, amount, invest_date, invest_term):
        try:
            invest_date = pd.to_datetime(invest_date).date()
            if ticker not in Stock_Prices.columns:
                pass  # Instruction to do nothing
            elif amount <= 0:
                pass  # Instruction to do nothing
            elif not isinstance(invest_term, int) or invest_term <= 0:
                pass  # Instruction to do nothing
            else:
                self.ticker = ticker                        # Ticker of the stock of interest for the investment
                self.amount = amount                        # Amount invested on the selected stock
                self.invest_date = adj_date(invest_date)    # Investment start date
                self.invest_term = invest_term              # Term of the investment (in days)
                self.is_valid = True                        # Check the investment validity
        except:
            # Error message
            print("This investment is not valid.")

    # "Get" functions (investment start date, investment end date, buying price, selling price, investment return, number of stocks)

    # Return the investment start date
    def getInvestStartDate(self):
        if self.is_valid:
            return self.invest_date

    # Return the investment end date
    def getInvestEndDate(self):
        if self.is_valid:
            return adj_date(self.getInvestStartDate() + timedelta(self.invest_term))

    # Return the price of the selected stock for investment, on the specified date (set as argument), after date adjustment
    def getPrice(self, date):
        if self.is_valid:
            return Stock_Prices.loc[adj_date(date), self.ticker]

    # Return the price of the selected stock on the investment start date
    # i.e. price at which stocks are bought on the investment start date
    def getBuyingPrice(self):
        if self.is_valid:
            return Stock_Prices.loc[self.getInvestStartDate(), self.ticker]

    # Return the price of the selected stock on the investment end date
    # i.e. price at which stocks are sold on the investment start date
    def getSellingPrice(self):
        if self.is_valid:
            return Stock_Prices.loc[self.getInvestEndDate(), self.ticker]

    # Return the investment return, with respect to buying and selling stock prices
    def getInvestmentReturn(self):
        if self.is_valid:
            return (self.getSellingPrice()/self.getBuyingPrice()) - 1

    # Return the number of shares bought on the investment start date,
    # with respect to the invested amount and the buying stock price
    def getNumberOfStocks(self):
        if self.is_valid:
            return (self.amount/self.getBuyingPrice())

    # Description ("print") function
    # Describe and print the main characteristics of the stock investment
    def describe(self):
        if self.is_valid:
            print(f"This investment is valid: ${self.amount} invested.\n{round(self.getNumberOfStocks(), 3)} of {self.ticker} stocks "
                  f"has been bought at ${round(self.getBuyingPrice(), 2)}/share on the {self.getInvestStartDate()} and has been sold at "
                  f"${round(self.getSellingPrice(), 2)}/share on the {self.getInvestEndDate()}.\n"
                  f"The investment return is: {round(self.getInvestmentReturn()*100, 2)}%.")
        else:
            print("This investment is not valid.")

