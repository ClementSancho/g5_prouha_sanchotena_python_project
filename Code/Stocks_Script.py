'''
This file contains several blocs of code to test the good implementation of Stock class.

In a first section, the user is invited to enter parameters of his choice for stock investments:
ticker, amount invested, investment start date and investment term.
The program ensure the selection of valid parameters
(eligible ticker, numerical amount and term (integer), valid investment date, etc.) in a robust way.

In a second section, the aim is to plot the stock price evolution of the available stocks over the entire allowed investment period,
i.e. from 2005-01-01 to 2020-12-31.
A first graph gather all stock price evolutions on the same graph and a second graph splits stock price graph for each individual stock.

'''


import matplotlib.pyplot as plt         # Import matplotlib library for plotting
from Stocks import Stock                # Import Stock class from Stocks.py file
from Stocks_Data import Stock_Prices    # Import stock prices dataframe from the file Stocks_Data.py
import pandas as pd                     # Import pandas library

'''
SECTION 1

Section dedicated to test the good instantiation of Stock class.

Example: 
   - Select the ticker of your choice among the following list: [FDX, GOOGL, IBM, KO, MS, NOK, XOM] (e.g. IBM)
   - Select a numerical and positive amount of your choice (e.g. $1000)
   - Select an investment start date (under the format mm/dd/yyyy) belonging to the allowed investment period 01/01/2005 to 01/01/2021 (e.g. 07/30/2018)
   - Select a numerical and positive term (in days) of your choice (e.g. 100 days)

'''

# Input management through interaction with the user and error handling

flag = False    # Flag variable to control the validity of inputs selected by the user

# Let the user select the stock (ticker) for the investment until he enters an eligible ticker.
while not flag:
    ticker = input(f"Eligible stocks: {Stock_Prices.columns.values}\nPlease select a stock (ticker) for your investment:")
    if ticker in Stock_Prices.columns:
        flag = True
    else:
        # Error message
        print("Selected ticker is not eligible." + '\n' + "Please select an eligible stock for your investment.")

flag = False

# Let the user select the amount of the investment until he enters a valid amount (numerical).
while not flag:
    amount = input("Please select an investment amount:")
    try:
        amount = float(amount)
        flag = True
    except:
        # Error message
        print("Selected amount is not valid." + '\n' + "Please select a numerical amount (float or integer).")

flag = False

# Let the user select the investment start date of the investment until he enters a valid date (mm/dd/yyyy format).
while not flag:
    invest_date = input(f"Allowed investment period: from {min(Stock_Prices.index)} to {max(Stock_Prices.index)}\n"
                        f"Please select an investment start date (mm/dd/yyyy format):")
    try:
        date = pd.to_datetime(invest_date).date()
        flag = True
    except:
        # Error message
        print("Selected investment start date is not valid.")


flag = False

# Let the user select the term of the investment until he enters a valid term (numerical).
while not flag:
    term = input("Please select an investment term (in days):")
    try:
        term = int(term)
        flag = True
    except:
        # Error message
        print("Selected term is not valid." + '\n' + "Please select a numerical term (integer only).")


# Instantiation of stock investment, with selected parameters

Stock1 = Stock(ticker, amount, invest_date, term)

Stock1.describe()


'''
SECTION 2

Plotting of the stock price path for each stock over the period 1/01/2005 to 1/01/2021

GRAPH 1  ->  Unique plot with all stock prices displayed on it

GRAPH 2  ->  Subplots of price evolution for each individual stock
'''

# GRAPH 1

plt.figure(figsize=(15,6))
plt.title("Stock prices over the period 9/01/2016 to 1/01/2021")
dates = Stock_Prices.index
for stock in Stock_Prices.columns:
    plt.plot(dates, Stock_Prices[stock], label=stock)
plt.xlabel('Date')
plt.ylabel('Stock Price ($)')
plt.xlim(Stock_Prices.index[0], Stock_Prices.index[-1])
plt.grid()
plt.legend()
plt.show()


# GRAPH 2

fig, axs = plt.subplots(4, 2)
axs[0, 0].plot(dates, Stock_Prices['FDX'], label='FDX', color='tab:blue')
axs[0, 0].set_title("FDX Stock price")
axs[0, 0].set_xlabel('Date')
axs[0, 0].set_ylabel('Stock Price ($)')

axs[1, 0].plot(dates, Stock_Prices['IBM'], label='IBM', color='tab:purple')
axs[1, 0].set_title("IBM Stock price")
axs[1, 0].set_xlabel('Date')
axs[1, 0].set_ylabel('Stock Price ($)')

axs[0, 1].plot(dates, Stock_Prices['GOOGL'], label='GOOGL', color='tab:cyan')
axs[0, 1].set_title("GOOGL Stock price")
axs[0, 1].set_xlabel('Date')
axs[0, 1].set_ylabel('Stock Price ($)')

axs[1, 1].plot(dates, Stock_Prices['KO'], label='KO', color='tab:orange')
axs[1, 1].set_title("KO Stock price")
axs[1, 1].set_xlabel('Date')
axs[1, 1].set_ylabel('Stock Price ($)')

axs[2, 0].plot(dates, Stock_Prices['MS'], label='MS', color='tab:red')
axs[2, 0].set_title("MS Stock price")
axs[2, 0].set_xlabel('Date')
axs[2, 0].set_ylabel('Stock Price ($)')

axs[2, 1].plot(dates, Stock_Prices['NOK'], label='NOK', color='tab:green')
axs[2, 1].set_title("NOK Stock price")
axs[2, 1].set_xlabel('Date')
axs[2, 1].set_ylabel('Stock Price ($)')

axs[3, 0].plot(dates, Stock_Prices['XOM'], label='XOM', color='tab:pink')
axs[3, 0].set_title("XOM Stock price")
axs[3, 0].set_xlabel('Date')
axs[3, 0].set_ylabel('Stock Price ($)')

axs[3, 1].set_visible(False)

fig.tight_layout()
plt.show()

