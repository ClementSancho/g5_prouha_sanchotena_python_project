'''
This file contains all the steps for processing the data of stock prices in order to build a functional dataframe
used for stock investments.

Stock prices are extracted from fr.finance.yahoo.com, and then imported as .csv files.

The different stocks under study are the following:
   - FDX (FedEx)
   - GOOGL (Alphabet, Google)
   - IBM (IBM)
   - KO (Coca-Cola Company)
   - MS (Morgan Stanley)
   - NOK (Nokia)
   - XOM (ExxonMobil)

In particular, daily "high" prices are under consideration, over the period from 2005-01-01 to 2020-12-31.

'''

import  numpy as np     # Import numpy library
import pandas as pd     # Import pandas library
import os               # Import operating system module


# Import stock prices data and business days data over the period 1/01/2005 to 12/31/2021 (collected on YahooFinance).

inputdataFDX = os.path.abspath('../Data/FDX.csv')
inputdataGOOGL = os.path.abspath('../Data/GOOGL.csv')
inputdataIBM = os.path.abspath('../Data/IBM.csv')
inputdataKO = os.path.abspath('../Data/KO.csv')
inputdataMS = os.path.abspath('../Data/MS.csv')
inputdataNOK = os.path.abspath('../Data/NOK.csv')
inputdataXOM = os.path.abspath('../Data/XOM.csv')
inputdataBDays = os.path.abspath('../Data/BusinessDays.csv')

dataFDX = pd.read_csv(inputdataFDX, sep=",")
dataGOOGL = pd.read_csv(inputdataGOOGL, sep=",")
dataIBM = pd.read_csv(inputdataIBM, sep=",")
dataKO = pd.read_csv(inputdataKO, sep=",")
dataMS = pd.read_csv(inputdataMS, sep=",")
dataNOK = pd.read_csv(inputdataNOK, sep=",")
dataXOM = pd.read_csv(inputdataXOM, sep=",")
dataBDays = pd.read_csv(inputdataBDays, sep=",")


# Process stock prices dataframes to keep only the 'high' prices (prices of interest for the project).
# Rename the vectors (series) by the respective name (ticker) of each stock.
dataFDX = pd.DataFrame(dataFDX['High']).rename(columns={'High':'FDX'})
dataGOOGL = pd.DataFrame(dataGOOGL['High']).rename(columns={'High':'GOOGL'})
dataIBM = pd.DataFrame(dataIBM['High']).rename(columns={'High':'IBM'})
dataKO = pd.DataFrame(dataKO['High']).rename(columns={'High':'KO'})
dataMS = pd.DataFrame(dataMS['High']).rename(columns={'High':'MS'})
dataNOK = pd.DataFrame(dataNOK['High']).rename(columns={'High':'NOK'})
dataXOM = pd.DataFrame(dataXOM['High']).rename(columns={'High':'XOM'})


# Business days data processing
# Convert dates from .csv file into Python datetime format
dataBDays['Date'] = dataBDays['Date'].map(lambda date : pd.to_datetime(date).date())

# Concatenation of dates and stock prices series created above in a unique dataframe
Stock_Prices = pd.concat([dataBDays, dataFDX, dataGOOGL, dataIBM, dataKO, dataMS, dataNOK, dataXOM], axis=1)

# Change the row indexing of Stock_Prices dataframe setting the corresponding business day as index
Stock_Prices.set_index('Date', inplace=True)






