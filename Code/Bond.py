'''
This file contains classes and their attributes and methods for bond investments.

The class Bond is a general (abstract) class.

ST_Bond (short-term) and LT_Bond (long-term) are two inherited classes from Bond.

DESCRIPTION

Short-term bonds (ST_Bond class) have:
   - a minimum investment amount of $250,
   - a minimum investment term of 2 years,
   - an annual interest rate of 1.5%,
   - a validity check which aims to characterize the validity or invalidity of the investment.

Long-term bonds (LT_Bond class) have:
   - a minimum investment amount of $1000,
   - a minimum investment term of 5 years,
   - an annual interest rate of 3%,
   - a validity check which aims to characterize the validity or invalidity of the investment.

A Bond investment will be considered as "valid" if:
   - the selected amount is numerical, and greater than the minimum required investment amount.
   - the selected investment term is a positive number (in years, can be integer or float),
     greater than the minimum required investment term.

Otherwise the investment is considered as "not valid".

For valid investments, value(t) method allows to get the value of the investment at any time t
(principal + earned interests by time t).

For valid investments, interests(t) method allows to get the amount of interests earned on the investment by time t.

Other "get" functions are provided, and a describe() function aims to print the characteristics of each bond
investment instantiation.

'''

# Bond (abstract) class

class Bond(object):

    # Declaration of common attributes shared by all bonds and initialization (arbitrarily set to zero)

    min_amount = 0      # Minimum investment amount allowed
    amount = 0          # Actual invested amount
    min_term = 0        # Minimum investment term allowed
    term = 0            # Actual investment term
    interest_rate = 0   # Annual interest rate
    nature = ""         # Nature of the bond: short-term or long-term
    is_valid = False    # Check the investment validity or invalidity


    # Constructor

    def __init__(self, amount, term):

        # Check of selected amount validity for the bond investment
        if (not isinstance(amount, float)) and (not isinstance(amount, int)):
            pass   # Instruction to do nothing
        elif (amount < self.min_amount):
            pass   # Instruction to do nothing

        # Check of selected term validity for the bond investment
        elif (not isinstance(term, float)) and (not isinstance(term, int)):
            pass   # Instruction to do nothing
        elif term < self.min_term:
            pass   # Instruction to do nothing

        else:
            self.amount = amount
            self.term = term
            self.is_valid = True   # Investment is considered as valid if and only if both amount and term are valid


    # Description ("print") function
    # Describe and print the main characteristics of the investment (type of the bond, initial amount invested, term)
    def describe(self):
        if self.is_valid:
            print(f"This investment is valid.\nIt is a {self.nature} bond, with an invested amount of ${round(self.amount, 2)},"
                  f" and an investment term of {self.term} years.")
        else:
            print("This investment is not valid.")


    # "Get" functions (nature, amount, term)

    # Return the nature of the bond (short-term, long-term or invalid)
    def getNature(self):
        if self.is_valid:
            return self.nature
        else:
            return "invalid"

    # Return the amount invested on the bond (0 by default if the investment is not valid)
    def getAmount(self):
        return self.amount

    # Return the term associated with the bond (0 by default if the investment is not valid)
    def getTerm(self):
        return self.term

    # Return the validity status of the bond (True if "valid", False if "not valid")
    def getIsValid(self):
        return self.is_valid


    # "Value(t)" and "Interests(t)" methods

    # "Value(t)" method takes a term t (duration in years, from the investment start date) as argument,
    # and returns the total value of the bond (incl. compounded interests) by time t
    # (0 by default if the investment is not valid)
    def value(self, t):
        if self.is_valid:
            return self.amount * (1 + self.interest_rate) ** t
        else:
            return 0


    # "Interests(t)" method takes a term t (duration in years, from the investment start date) as argument,
    # and returns the total amount of interests earned on the bond investment by time t
    # (0 by default if the investment is not valid)
    def interests(self, t):
        if self.is_valid:
            return self.value(t) - self.amount
        else:
            return 0


# ST_Bond class for short-term bond investments (inherited from Bond class)

class ST_Bond(Bond):

    # Initialisation of attributes for short-term bonds

    min_amount = 250          # Minimum investment amount for short-term bonds: $250
    min_term = 2              # Minimum investment term for short-term bonds: 2 years
    interest_rate = 0.015     # Annual interest rate for short-term bonds: 1.5%
    nature = "short-term"


# LT_Bond class for long-term bond investments (inherited from Bond class)

class LT_Bond(Bond):

    # Initialisation of attributes for long-term bonds

    min_amount = 1000         # Minimum investment amount for long-term bonds: $1000
    min_term = 5              # Minimum investment term for long-term bonds: 5 years
    interest_rate = 0.03      # Annual interest rate for long-term bonds: 3%
    nature = "long-term"